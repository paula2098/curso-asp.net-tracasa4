﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Usuarios_CRUD_WCF.Controllers
{

    [ApiController]
    [Route("/front")]
    public class HomeController : Controller
    {
        [HttpGet("/front/{filename}")]
        public byte[] GetFileFromFolder(string filename)
        {
            byte[] filedetails = new byte[0];
            string strTempFolderPath = System.Configuration.ConfigurationManager.AppSettings.Get("FileUploadPath");
            Console.WriteLine("strTempFolderPath:" + strTempFolderPath);
            string directorio = System.IO.Path.GetFullPath("./www/");
            
            if (System.IO.File.Exists(directorio +  filename))
            {
                return System.IO.File.ReadAllBytes(directorio + filename);
            }
            else 
                return filedetails;
        }
    }
}
