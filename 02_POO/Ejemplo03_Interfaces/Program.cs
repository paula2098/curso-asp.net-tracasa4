﻿using Ejemplo01_Encapsulacion;
using Ejemplo02_Herencia;
using Ejercicio01_Encapsulacion;
using System;


namespace Ejemplo03_Interfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            // Por lo general no es aconsejable crear un array de objetos
            // OJO: Esto es sólo a nivel educativo
            // Lo normal es declarar un array de clases, interfaces ó  clases abstractas
            object[] popurri = new object[3];
            popurri[0] = new Coche("Fiat", "Punto", 9000);
            popurri[1] = new CocheElectrico("Fiat", "Punto", 9000);
            popurri[2] = new Usuario("Fulanito", 50, 2);

            foreach (object objQueSea in popurri)
            {
                Console.WriteLine(objQueSea.ToString());
            }
            ((Coche)popurri[0]).SetNombre("FIAT - PUNTO 4.5");
            Coche fiatPunto = (Coche) popurri[0];

            Console.WriteLine(fiatPunto.GetNombre().ToUpper().Trim());

            // El polimorfismo se puede usar con interfaces
            INombrable fiatNombrable = /*(INombrable)*/ fiatPunto;
            fiatNombrable.Nombre = "Fiat - Punto Version 1034";
            Console.WriteLine(fiatNombrable.Nombre.Trim());

            INombrable cen = (CocheElectrico)popurri[1];
            Console.WriteLine(cen.GetType().Name + ":" + cen.GetNombre());
            /*
            char[] caract = {'A', 'B', 'C'};
            string letra = (" " + caract[1]).ToUpper();
            char caracterSolito = letra.ToCharArray()[0];
            char caracterSolito1 = letra.ToCharArray()[1];
            */
        }
    }
}
/* 1 - Comprobar si Coche Electrico puede hacerse polimorfismo con INom...
 * 2 - Podeis un nuevo proyecto Ejercicio03_interfaces
 *      Hacer que el usuario que ya tenemos implemente la interfaz 
 *      INombrable y usar los 2 métodos y la propiedad
 * 3 -  Crear un array de INombrable con un empleado y su coche electrico
 * 4 -  ICloneable YA EXISTE EN .Net
 *      Implementar dicha interfaz en CocheElectrico:
 *      El método tiene que instanciar un nuevo obj y asignar 
 *      las propiedades del nuevo coche con sus propias propiedades.
 *      Es decir, se clona a sí mismo
 * 5 - Crear una nueva interfaz que obligue a implementar
 *      2 métodos, uno para mostrar directamente los datos por consola
 *      y otro para pedir sus datos por consola.
 * 6 -  Implementar dicha interfaz en Usuario y en Coche
 *      sobreescribir los métodos en Empleado (si quereis en CElectrico)
 * 7 -  Usar los métodos en un nuevo usuario y en el empleado del 
 *      ejercicio 3, y en un Coche (y si quereis CocheElectrico)
 */