﻿using System;

namespace Ejercicio01_Encapsulacion
{

    /* Nuevo Proyecto Ejercicio01_Encapsulacion
     *  Crear una clase Usuario en otro fichero, con nombre, edad, y altura, 
     *  pero con  las variables miembro encapsuladas como propiedades
     *  Nombre no puede ser ni null ni "". En su lugar "SIN NOMBRE"
     *  edad debe ser mayor que 0
     *  Altura mínima de 0.1F metros
     *  
     *  Crear un pequeño código para comprobar que funciona:
     *      Con casos que funcione bien y casos con valores prohibidos
     */
    public class Usuario : Object
    {
        private string nombre;
        private int edad;
        private float altura;

        public Usuario(string nombre, int edad, float altura)
        {

            this.Nombre = nombre;
            this.Edad = edad;
            this.Altura = altura;

        }

        public Usuario()
        {

            Nombre = " ";
            Edad = 0;
            Altura = 1.0f;

        }


        public string Nombre
        {
            get
            {
                return nombre;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    nombre = "SIN NOMBRE";
                else
                    nombre = value;
            }
        }
        public int Edad
        {
            get
            {
                return edad;
            }
            set
            {
                edad = value <= 0 ? 1 : value;
            }
        }
        public float Altura
        {
            get
            {
                return altura;
            }
            set
            {
                altura = value <= 0.1f ? 0.1f : value;
            }
        }

        

        public string GetNombre()
        {
            return Nombre;
        }

        public void SetNombre(string unNombre)
        {
            if (!string.IsNullOrEmpty(unNombre))
            {

                string[] separados = unNombre.Split(" ");
                Nombre = separados[0].Trim();

            }
        }

        public override string ToString()
        {
            return "Nombre: " + nombre + "  Edad: " + edad + " años  Altura: " + altura + " metros";
        }

        public virtual void MostrarDatos()

        // Mostramos datos del usuario

        {
            Console.WriteLine(this.ToString());
        }

        public virtual void PedirDatos()
        {
            // Pedir al usuario los datos

            Console.WriteLine("Introducir datos del usuario. ");


            Console.WriteLine("Nombre: ");
            this.Nombre = Console.ReadLine();

            Console.WriteLine("Edad: ");
            this.Edad = int.Parse(Console.ReadLine());

            Console.WriteLine("Altura: ");
            this.Altura = float.Parse(Console.ReadLine());

        }
       

    }
}
