﻿using System;

namespace Ejemplo01_Encapsulacion
{
    class Program
    {
        static void Main(string[] args)
        {
            Coche miCoche = new Coche();
            // Por defecto es privado, no podemos (ni debemos) modif directamente
            // miCoche.velocidad = 10;
            miCoche.Acelerar();
            miCoche.Acelerar();
            // Seguimos sin poder modificar el valor
            // miCoche.Velocidad = 100;
            miCoche.Modelo = "Kia";

            Console.WriteLine("Velocidad: " + miCoche.Velocidad);
            Console.WriteLine("Modelo: " + miCoche.Modelo);

            (new Coche("Coche temporal", "Temp", 10)).Acelerar();
        }
        
    }
}
