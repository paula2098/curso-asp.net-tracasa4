﻿using Ejemplo03_MVC.DLL.Modelo;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo02_TestMVC
{
    class TestModeloEjemplo
    {
        [SetUp]
        public void SU()
        {

        }
        [Test(Author ="German", Description = "Probando Modelo Ejemplo")]
        public void TestModeloListaEjemplo()
        {
            IModeloEjemplo model1; 
            model1 = new ModeloEjemploLista();

            model1.Crear(1, "Uno");
            model1.Crear(2, "Dos");
            model1.Crear(3, "Tres");

            Assert.AreEqual(model1.LeerTodos().Count, 3);
        }
    }
}
