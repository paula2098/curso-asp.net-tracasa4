using NUnit.Framework;
using System;

namespace _01_ProyectoTests
{
    public class Tests
    {
        string texto;
        string myString;
        // Este es el metodo de inicializacion porque lleva el  atributo c#[SetUp]
        // Los atributos son decoradores, caracteristicas que a�adimos a clases, propiedades y metodos
        // Para agregar cuerta funcionalidad 

        [SetUp]
        public void Inicializacion()
        {

            texto = "Texto inicial";
        }

        [Test]
        public void TestPrimero()
        {
            // Assert : asegurar
            Assert.Pass();
        }

        [Test]
        public void TestSegundo()
        {
            // Assert : asegurar
            Assert.AreEqual(1 + 3, 2 + 2);
            Assert.AreNotEqual(1 + 3, 2 + 0);
            Assert.IsNotNull(this.texto, "Texto es nulo");
            Assert.AreEqual("Clean", texto, "El texto no se ha limpiado correctamente");
            texto = "Texto inicial 2";


        }

        public static void DelegadoCualquieraOk()
        {
            Console.WriteLine("Delegado cualquiera ok");
        }
        public static void DelegadoCualquieraMal()
        {
            throw new Exception("Delegado cualquiera MAL");
        }



        [Test]
        public void Test3()
        {
            Console.WriteLine("Antes del test 3");
            Assert.IsTrue(texto.Equals("Texto inicial"), "El texto no se ha inicializado");
            Assert.Contains("i", texto.ToCharArray(), "el array de char texto no tiene 'i'");
            TestDelegate delegateOk = DelegadoCualquieraOk;
            Assert.DoesNotThrow(delegateOk, "Delegado cualquiera Ok FALL�");
            Assert.Throws<NotImplementedException>(DelegadoCualquieraMal, "Delegado cualquiera Ok CASC�");
            Console.WriteLine("Despues del test 3");

            // Assert.That();
            //  Assert.That(myString, Is.EqualTo("Hello"));

        }

        [Test(Author = "Paula", Description = "Assert.That: Se asegura de que una condici�n es verdadera. Si la condici�n es falsa, AssertionException.")]
        public void Test_Assert_That()
        {
            //Assert.That(bool condition);
            Assert.That(5 == 5);

            // Con mensaje de error si la condici�n no es true
            Assert.That(5 == 6, "Es falso");

            // Assert.That(myString, Is.EqualTo("Hello"));
            Assert.That("Hola", Is.EqualTo("Hello").Or.EqualTo("Hola"));

        }

    }
}