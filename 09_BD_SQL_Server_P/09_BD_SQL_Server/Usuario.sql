﻿CREATE TABLE [dbo].[Usuario]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [nombre] VARCHAR(50) NOT NULL, 
    [edad] TINYINT NOT NULL, 
    [altura] FLOAT NOT NULL, 
    [activo] BIT NULL
)
