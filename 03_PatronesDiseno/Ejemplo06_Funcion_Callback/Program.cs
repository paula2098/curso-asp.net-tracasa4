﻿using System;

namespace Ejemplo06_Funcion_Callback
{

    // Creamos un nuevo tipo de dato, que indica que es una funcion
    // (ni clase, ni interfaz..). Donde lo que imprta son los tipos de dato
    // que recibe y que devuelve

    // Es decir, declaramos un delegado
    delegate float FuncionOperador(float op1, float op2);

    class Program
    {
        static void Main(string[] args)
        {
            VistaCalculadora(Calculadora_A.SumarA);
            Console.WriteLine("Invocamos con multiplicador");
            VistaCalculadora(Calculadora_B.MultiplicarB);
        }

    
        static void VistaCalculadora(FuncionOperador funSuma, )
        {
            Console.WriteLine("Introduzca la operación: ");

            string oper = Console.ReadLine();
            Console.WriteLine("Operando 2: ");
            int posOp = oper.IndexOf("+");
            
            if (posOp < 0)
            {
                posOp = oper.IndexOf("*");
                if (posOp < 0 )
                {
                    Console.WriteLine("No te entiendo");
                    return;
                }
            }
            else
            {
                char caracterOp = oper.Substring(posOp, 1)[0];
            }


            float x = float.Parse(oper.Substring(0, posOp));
            float y = float.Parse(oper.Substring(posOp+1));
            if (caracterOp =='+')
            {

            }
            float resultado = operador(x, y);
        }

        // Esta funcion necesita una funcion callback para
        // saber como tiene que calcular
        /*static void VistaCalculadora(FuncionOperador operador)
         {
             Console.WriteLine("Operando 1: ");
             float x = float.Parse(Console.ReadLine());
             Console.WriteLine("Operando 2: ");
             float y = float.Parse(Console.ReadLine());
             float resultado = operador(x, y);
             Console.WriteLine("Resultado: " + resultado);

         }*/


        // Ejercicio1: Usar VistaCalculadora con  una calc creada por vosotros, pero exacta
        // Llamad a la clase CalculadoraB

        //Ejercicio2 : Crear una Vista operadora (fun estatica) que el usuario pueda introducir
        // una unica cadena con dos numeros y el operador. Que calcule uysando 
        // CalcA o calcB u otra diferente. Si lo pone mal que diga
        // "No entiendo, humano":
        // Ejemplos: 10+22 o 333*1 ó 2.5*11
    }
}
