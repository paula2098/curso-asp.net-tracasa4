﻿using System;
namespace Ejemplo01_Encapsulacion
{


 /*Interfaz que contiene dos métodos:
  * - MostrarDatos() por consola
  * - PedirDatos() por consola
*/

    public interface IPedirMostrar
    {

        void MostrarDatos();

        void PedirDatos();

    }
}
