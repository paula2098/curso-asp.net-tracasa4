﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Ejemplo03_MVC.Aplicacion.Modelo;
using Ejemplo03_MVC.Aplicacion.Modelo.Interfaces;
using NUnit.Framework;

namespace Ejemplo03_MVC.Test
{
    class TestModeloEjemplo
    {
        [Test]
        public void TestModeloListaEjemplo()
        {
            IModeloEjemplo model1;
            model1 = new ModeloEjemploLista();

            model1.Crear(1, "uno");
            model1.Crear(2, "dos");
            model1.Crear(3, "tres");


            ArrayList lista = new ArrayList((ICollection)model1.LeerTodos());
            Assert.AreEqual(model1.LeerTodos().Count, 3);
        }
    }
}
