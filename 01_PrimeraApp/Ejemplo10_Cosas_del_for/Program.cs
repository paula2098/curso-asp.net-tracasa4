﻿using System;

namespace Ejemplo10_Cosas_del_for
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 0; i < 5; i ++)
            {
                Console.WriteLine("Num " + i);
            }
            // Eso es lo mismo que
            int j = 0;
            while (j < 5)
            {
                Console.WriteLine("Num " + j);
                j++;
            }
            // Por lo tanto podemos hacer
            int k = 0;
            for ( ; k < 5; )
            {
                Console.WriteLine("Num " + k);
                k++;
            }
            // Por ejemplo, podemos por ejemplo, usarlo como while porque es un while
            bool salir = false, otra;
            for (Console.WriteLine("Empezamos!"); ! salir; salir = bool.Parse(Console.ReadLine()))
            {
                Console.WriteLine("¿Salimos? ");
            }
        }
    }
}
