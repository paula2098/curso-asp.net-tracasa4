const {Builder, By, Key, until} = require('selenium-webdriver');

const firefox = require('selenium-webdriver/firefox');

(async function example() {

  let options = new firefox.Options();
  let strFFBinaryPath = "C:/Users/pmpcurso1/AppData/Local/Mozilla Firefox/firefox.exe";
  options.setBinary(strFFBinaryPath);
  let driver = await new Builder().forBrowser('firefox').setFirefoxOptions(options).build();
  try {
    await driver.get('http://www.duckduckgo.com/');
    await driver.findElement(By.name('q')).sendKeys('webdriver', Key.RETURN);
    await driver.wait(until.titleIs('webdriver at DuckDuckGo'), 2000);
  } finally {
    //await driver.quit();
  }
})();