/* JavaScript no tiene clases. Es un lenguaje prototípico. Basado en 
prototipos. */
window.onload = function() {
    let btnAnadir = document.getElementById("btn-anadir");
    let usuario = JSON.parse(window.localStorage.getItem("mi-usuario"));
    console.log("Cargado: " + JSON.stringify(usuario));

    let filaTabla  = 
` <tr>
<td>${usuario.nombre}</td>
<td>${usuario.edad}</td>
<td>${usuario.altura}</td>
</tr>` 
document.getElementById("tbody-usuarios").innerHTML = filaTabla;

    btnAnadir.onclick = function() {
        let campoNombre = document.getElementById("nombre");
        campoNombre.value = campoNombre.value.toUpperCase();
        let nombre = campoNombre.value;
        let edad = parseInt(document.getElementById("edad").value);
        let altura = parseFloat(document.getElementById("altura").value);

        // Aquí, con new, instanciamos una copia del Object prototipo
        let aficiones =  new Object();
        aficiones.leer = document.getElementById("aficiones-leer").checked;
        aficiones.musica = document.getElementById("aficiones-musica").checked;
        aficiones.cine = document.getElementById("aficiones-cine").checked;

        // Usamos notación JSON (JavsScript Object Notation)
        // las llaves son como new Object();
        let usuario = {
            "nombre": nombre,
            "edad": edad,
            "altura": altura,
            "aficiones": aficiones
        }
        window.localStorage.setItem("mi-usuario", JSON.stringify(usuario));
        window.location = window.location;
    };
    // Confiamos en que lo que llegue es un usuario
    function textoAlertUsuario(usu) {
        return `Yepa ${usu.nombre.toLowerCase()}  tienes ${usu.edad} y altura ${usu.altura} te gusta ${usu.aficiones.leer ? " leer " : ""} ${usu.aficiones.musica ? " musica " : ""} ${usu.aficiones.cine ? " cine " : ""}!`
    }
    document.getElementById("btn-ir").addEventListener("click", function() {
        window.location = document.getElementById("url").value;
    });
};