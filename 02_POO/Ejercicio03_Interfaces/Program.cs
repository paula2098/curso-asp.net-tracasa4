﻿using System;
using Ejemplo01_Encapsulacion;
using Ejercicio01_Encapsulacion;

using Ejemplo02_Herencia;
using Ejercicio02_Herencia;
using Ejemplo03_Interfaces;

/* 1 - Comprobar si Coche Electrico puede hacerse polimorfismo con INom...
 * 
 * 2 - Podeis un nuevo proyecto Ejercicio03_interfaces
 *      Hacer que el usuario que ya tenemos implemente la interfaz 
 *      INombrable y usar los 2 métodos y la propiedad
 *      
 * 3 -  Crear un array de INombrable con un empleado y su coche electrico
 * 
 * 4 -  ICloneable YA EXISTE EN .Net
 *      Implementar dicha interfaz en CocheElectrico:
 *      El método tiene que instanciar un nuevo obj y asignar 
 *      las propiedades del nuevo coche con sus propias propiedades.
 *      Es decir, se clona a sí mismo
 *      
 * 5 - Crear una nueva interfaz que obligue a implementar
 *      2 métodos, uno para mostrar directamente los datos por consola
 *      y otro para pedir sus datos por consola.
 *      
 * 6 -  Implementar dicha interfaz en Usuario y en Coche
 *      sobreescribir los métodos en Empleado (si quereis en CElectrico)
 *      
 * 7 -  Usar los métodos en un nuevo usuario y en el empleado del 
 *      ejercicio 3, y en un Coche (y si quereis CocheElectrico)
 */


namespace Ejercicio03_Interfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            // Hacer que el usuario que ya tenemos implemente la interfaz 
            // INombrable y usar los 2 metodos y la propiedad

            Usuario us1 = new Usuario();
            us1.SetNombre("Paula");
            Console.WriteLine(us1.GetNombre());

            // INombrable
            INombrable usN = (INombrable)us1;
            usN.Nombre = "Paula2";
            Console.WriteLine(usN.Nombre);

            // Array de INombrable con un empleado y su coche eléctrico

            object[] lista = new object[2];

            lista[0] = new Empleado("María", 18, 1.70f, 23200);
            lista[1] = new CocheElectrico("Opel", "Corsa", 2000, 99);

            INombrable emN = (INombrable)lista[0];
            INombrable cEN = (INombrable)lista[1];
            /*
            Console.WriteLine(emN);
            Console.WriteLine(cEN);
            */


            // Implementar intefaz en usuario y coche

            Usuario usuarioPrueba = new Usuario();
            usuarioPrueba.PedirDatos();
            usuarioPrueba.MostrarDatos();

            Coche cochePrueba = new Coche();
            cochePrueba.PedirDatos();
            cochePrueba.MostrarDatos();


        }
    }
}
