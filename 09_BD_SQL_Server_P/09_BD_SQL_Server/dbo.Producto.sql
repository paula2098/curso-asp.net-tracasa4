﻿CREATE TABLE [dbo].[Producto]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY (1,1), 
    [Nombre] VARCHAR(50) NOT NULL, 
    [Precio] DECIMAL(9, 2) NOT NULL
)
