﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ejercicio06_Callback

{

    // declaracion delegado para calcular operaciones
    delegate float Calcular(float op1, float op2);
    static class CalculaArrays
    {
        // Metodos para operar con dos float
       /*public static float Suma(float op1, float op2)
        {
           return (op1 + op2);
        }*/

      /* public static float Resta(float op1, float op2)
        {
            return (op1 - op2);
        }

        public static float Multiplicacion(float op1, float op2)
        {
            return (op1 * op2);
        }

        public static float Division(float op1, float op2)
        {
            return (op1 / op2);
        }*/


        // Metodos de suma, resta, multiplicacion y division antes de simplificarlos
        // Comprobar si el array estaba vacip
        /*static float Sumar(float[] x)
        {

            float res = 0;
            for (int i = 0; i < x.Length; i++)
            {
                float a = x[i];
                res = res + a;
            }
            return res;
        }

        static float Restar(float[] x)
            {
                float res = 0;
                for (int i = 0; i < x.Length; i++)
                {
                    float a = x[i];
                    res = res - a;
                }
                return res;
            }


            static float Dividir(float[] x)
            {
                float res = 0;
                for (int i = 0; i < x.Length; i++)
                {
                    float a = x[i];
                    res = res / a;
                }
                return res;
            }

            static float Multiplicar(float[] x)
            {
                float res = 0;
                for (int i = 0; i < x.Length; i++)
                {
                    float a = x[i];
                    res = res * a;
                }
                return res;
            }
        */

        // Metodo OpGenerica para realizar la operacion tanto si es suma, como resta, multplicacion o division
        static float OpGenerica(float[] arrayOp, Calcular calculo)
        {
               float total = 0;

               for (int i = 0; i < arrayOp.Length; i++){ // Recorremos el array
                                                         // 
                    if (i==0) total = arrayOp[i]; // si i es 0, Result = primer valor de i

                    else total = calculo (total, arrayOp[i]); 
                } 
                return total;

        }


        // Dependiendo de la eleccion que realice el usuario por pantalla, realizamos la operacion adecuada llamando
        // al metodo OpGenerica
        public static float OperacionCompleta(float[] arrayOp, int elec)
        {
            switch (elec)
            {
                // OpGenerica realiza la suma de los elementos del array
                case 1:

                    return OpGenerica(arrayOp, (float op1, float op2) => (op1 + op2)); // Funcion lambda para calcular la suma de dos float
                // OpGenerica realiza la resta de los elementos del array
                case 2:

                    return OpGenerica(arrayOp, (float op1, float op2) => (op1 - op2)); // Funcion lambda para calcular la resta de dos float
                // OpGenerica realiza la multiplicacion de los elementos del array
                case 3:

                    return OpGenerica(arrayOp, (float op1, float op2) => (op1 * op2)); // Funcion lambda para calcular la multiplicacion de dos float
                // OpGenerica realiza la división de los elementos del array
                case 4:

                    return OpGenerica(arrayOp, (float op1, float op2) => (op1 / op2)); // Funcion lambda para calcular la division de dos float

            }
           return 0 ;
        }




    }


}

