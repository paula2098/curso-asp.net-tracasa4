﻿using System;

namespace Ejemplo08_If_Operadores_Logic
{
    class Program
    {
        static void Main(string[] args)
        {
            
            int num1, num2, num3, num4;
            
            string linea;
            Console.Write("Ingrese primer valor:");
            linea = Console.ReadLine();
            num1 = int.Parse(linea);
            Console.Write("Ingrese segundo valor:");
            linea = Console.ReadLine();
            num2 = int.Parse(linea);
            Console.Write("Ingrese tercer valor:");
            linea = Console.ReadLine();
            num3 = int.Parse(linea);
            Console.Write("Ingrese 4to valor:");
            linea = Console.ReadLine();
            num4 = int.Parse(linea);
            if (num1 > num2 && num1 > num3 && num1 > num4)
            {
                Console.Write(num1);
            }
            else
            {
                if (num2 > num3 && num2 > num4)
                {
                    Console.Write(num2);
                }
                else
                {
                    if (num3 > num4)
                    {
                        Console.Write(num3);
                    }
                    else
                    {
                        Console.Write(num4);
                    }
                }
            }
            Console.ReadKey();
        }
    }
}
