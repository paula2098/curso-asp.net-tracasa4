﻿using Ejemplo06_CSharp_Avanzado.MetodosExt;
using System;

namespace Ejemplo06_CSharp_Avanzado
{
    class Program
    {
        static void Main(string[] args)
        {
            
                Fruta esLaPera = new Fruta("La pera repera", 150.01f);
                Console.WriteLine(esLaPera.ToString());
                Console.WriteLine(esLaPera.FormatearNombre());
                Console.WriteLine(ModuloMetodosExtension.FormatearNombreN(esLaPera));
                Console.WriteLine(esLaPera.ToUpperString());
                object otroObjeto = new object();
                Console.WriteLine(otroObjeto.ToUpperString());
                int[] otroArray = new int[] { 3, 2, 1 };
                Console.WriteLine(otroArray.ToUpperString());
                string[] otroArrayStr = new string[] { "tres", "dos", "uno" };
                Console.WriteLine(otroArrayStr.ToUpperString());
                // Prueba con Frutas
                Fruta Manzana = new Fruta("Manzana", 150.02f);
                Fruta Kiwi = new Fruta("Kiwi", 176.01f);
                Fruta Platano = new Fruta("Platano", 196.01f);
                Fruta Melon = new Fruta("Melon", 206.01f);

                Fruta[] frutas = new Fruta[] { esLaPera, Manzana, Kiwi, Platano, Melon };
                Console.WriteLine(frutas.ToUpperString());

            // Ejercicio Metodo de extension de array, que sea array.ToStringFiltrado(string cadena);
            // que solo muestre los ToString() que contengan cadena, sin tener en cuenta mayus o minus
            // Ej: otroArrayStr.ToStringFiltrado("s") tres dos
            // Ej: frutas.ToStringFiltrado ("p") esLaPera, Piña Platano

                Console.WriteLine(frutas.ToUpperFiltrado("p"));


        }
    }

    
}

