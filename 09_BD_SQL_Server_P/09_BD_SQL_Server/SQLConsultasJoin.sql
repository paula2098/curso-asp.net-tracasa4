﻿-- El conjunto común de datos
/*
SELECT	U.Id, U.email, 
		C.Id, C.Marca
	FROM Usuario as U
	INNER JOIN Coche as C
	ON U.idCoche = C.Id
	*/
-- Todos los usuarios y si tienen coche, con su coche. Y si no, se rellena lo del coche como NULL
/*SELECT	U.Id, U.email, 
		C.Id, C.Marca
  FROM Usuario as U
			LEFT JOIN Coche as C
	ON U.idCoche = C.Id */

-- Todos los coches con sus usuarios si los tuviera, y si no también
/*SELECT	U.Id, U.email, 
	C.Id, C.Marca
	FROM Usuario as U
		RIGHT JOIN Coche as C
ON U.idCoche = C.Id
*/
-- Todo con todo aunque no tengan combinación

/*SELECT	U.Id, U.email, 
	C.Id, C.Marca
	FROM Usuario as U
		FULL OUTER JOIN Coche as C
ON U.idCoche = C.Id
*/


   
/*
	 select SUM(c.Cantidad) from Producto as P, CompraUsuarioProducto as C, Usuario as U 
			where C.IdUsuario = U.Id and C.IdProducto = P.Id and U.edad <= 20 
		1 - Esta consulta con JOIN
		
	Por cada usuario, nombre de usuario y nombre de producto,
		2 - El precio del producto de sus compras
		3 - Cuanto se ha gastado en total en cada producto
		4 - Cuantas copias del producto ha comprado
	Por otro lado:
		5 - Todas las ventas (dinero) del 2020
		6 - Todos los usuarios que compraron en el 2019
		7 - Qué productos se han vendido mejor a lo largo del 2018, 2019 y 2020
		8 - El coche cuyo propietario ha comprado más a lo largo de la historia
		9 - La media de edad de los compradores de cada uno de los productos (melón, cuajada...)
		10 - Qué productos han comprado los usuarios del Mercedes
*/

--2 - El precio del producto de sus compras
SELECT u.nombre, p.precio
FROM  Usuario u inner join CompraUsuProducto c on u.Id = c.IdUsuario inner join Producto p on c.IdProducto = p.Id


--3 - Cuanto se ha gastado en total en cada producto

SELECT u.nombre, p.Nombre , sum(c.Cantidad * p.Precio) as PRECIO_POR_PRODUCTO
FROM Usuario u left join CompraUsuProducto c on u.Id = c.IdUsuario right join Producto p on c.IdProducto = p.Id
group by u.nombre, p.nombre

--4 - Cuantas copias del producto ha comprado

SELECT  u.nombre, c.Cantidad
FROM Usuario u inner join CompraUsuProducto c on u.Id = c.IdUsuario inner join Producto p on c.IdProducto = p.Id;


--5 - Todas las ventas (dinero) del 2020

SELECT *
FROM CompraUsuProducto c inner join Producto p on c.IdProducto = p.Id
WHERE c.Fecha  between '2020-01-01' and '2020-12-31'


--6 - Todos los usuarios que compraron en el 2019

SELECT u.nombre
FROM usuario u inner join CompraUsuProducto c on u.Id = c.IdUsuario inner join Producto p on c.IdProducto = p.Id
where c.Fecha  between '2019-01-01' and '2019-12-31'


--7 - Qué productos se han vendido mejor a lo largo del 2018, 2019 y 2020














SELECT *
FROM	(SELECT p.id, p.Nombre, max (c.Cantidad) as MAX_AÑO, YEAR (c.Fecha) as AÑO
		FROM CompraUsuProducto c inner join Producto p on c.IdProducto = p.Id
		group by c.Fecha, p.Nombre, p.Id) AS SUB;






select top 1 * 
from (SELECT p.Nombre, max (c.Cantidad) as MAX_AÑO, YEAR (c.Fecha) as AÑO
	FROM CompraUsuProducto c inner join Producto p on c.IdProducto = p.Id
	where YEAR(c.Fecha) = 2018 Or YEAR(c.Fecha) = 2019 Or YEAR(c.Fecha) = 2020
	group by c.Fecha, p.Nombre) as sub;


SELECT  c.cantidad, p.Nombre, max (c.Cantidad) as MAX_AÑO, YEAR (c.Fecha) as AÑO
FROM CompraUsuProducto c inner join Producto p on c.IdProducto = p.Id
group by c.Fecha, p.Nombre
order by MAX_AÑO desc;


SELECT top 1 p.Nombre, max (c.Cantidad) as MAX_AÑO, YEAR (c.Fecha) as AÑO
FROM CompraUsuProducto c inner join Producto p on c.IdProducto = p.Id
where YEAR(c.Fecha) = 2019
group by c.Fecha, p.Nombre
order by MAX_AÑO desc;

select *
	SELECT top 1 p.Nombre, max (c.Cantidad) as MAX_AÑO, YEAR (c.Fecha) as AÑO
	FROM CompraUsuProducto c inner join Producto p on c.IdProducto = p.Id
	where YEAR(c.Fecha) = 2020
	group by c.Fecha, p.Nombre
	order by MAX_AÑO desc


SELECT * FROM (
    select top 5  * from [dbo].[Usuario] where coche is null order by edad desc
 ) AS subconsulta_5 ORDER BY nombre

--8 - El coche cuyo propietario ha comprado más a lo largo de la historia

SELECT max(com)
FROM coche c inner join usuario u on c.Id = u.idCoche inner join CompraUsuProducto co on u.id = co.IdUsuario inner join Producto p on co.IdProducto = p.Id;


--9 - La media de edad de los compradores de cada uno de los productos (melón, cuajada...)

SELECT avg (u.edad), p.id
FROM Usuario u inner join CompraUsuProducto c on u.Id = c.IdUsuario inner join Producto p on c.IdProducto = p.Id
group by p.id;

--10 - Qué productos han comprado los usuarios del Mercedes

SELECT p.Nombre
FROM  coche c inner join usuario u on c.Id = u.idCoche inner join CompraUsuProducto co on u.id = co.IdUsuario inner join Producto p on co.IdProducto = p.Id
WHERE c.marca = 'Mercedes'

-- CUANTOS USUARIOS TIENEN CADA COCHE

SELECT C.MARCA
FROM COCHE  C INNER JOIN USUARIO U ON U.IDCOCHE = C.ID
GROUP BY C.MARCA;

