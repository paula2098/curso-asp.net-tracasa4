﻿-- select * from [dbo].[Usuario] where edad > 40

-- select * from [dbo].[Usuario] where activo = 'True'
-- select * from [dbo].[Usuario] where activo = 0 Or edad >=30
-- select * from [dbo].[Usuario] where not (activo = 0 Or edad >=33)
-- select * from [dbo].[Usuario]order by edad desc, activo asc
 -- select * from [dbo].[Usuario] where coche is not null
--  select top 5 * from [dbo].[Usuario] where coche is null order by edad desc
  -- Devolver 5 usuarios de mayor edad sin coche

 -- SELECT *FROM(
	--select top 5 * from [dbo].[Usuario] where coche is null order by edad desc
--	) AS subconsulta_5 order by nombre

-- Devolver cuantos hay inactivos y cuantos activos

select count ( distinct activos.id), count (distinct inactivos.id)
	from (select * from usuario where activo = 0)  as activos,
		 (select * from usuario where activo = 1) as inactivos


-- Devuelve 3 usuariios con menor altura ord por edad

select top 3 * 
from [dbo].[Usuario]  
order by altura asc, edad desc

-- devuelve los usuarios con el mismo coche
select *
from Usuario as usu1, Usuario as usu2 
where usu1.coche = usu2.coche  AND usu1.Id < usu2.Id

-- Devolver el usuario con mas edad con coche
SELECT TOP 1 *
FROM Usuario
WHERE COCHE IS NOT NULL
ORDER BY EDAD DESC

-- Devolver el usuario de menos edad inactivo y sin coche
SELECT TOP 1 *
FROM Usuario
WHERE COCHE IS NULL AND ACTIVO = 0 
ORDER BY EDAD ASC

SELECT COUNT (*) AS CANTIDAD,
	CASE WHEN COCHE IS NOT NULL THEN COCHE ELSE 'SIN COCHE' END
	FROM USUARIO
	GROUP BY COCHE
	ORDER BY CANTIDAD


-- mEDIA DE EDADES DE LOS USUARIOS ACTIVOS, INACTIVOS, CON COCHE Y SIN COCHE


SELECT AVG (EDAD) AS MEDIA_INACTIVO
FROM Usuario 
WHERE ACTIVO = 0

SELECT AVG (EDAD) AS MEDIA_SIN_COCHE
FROM Usuario 
WHERE COCHE IS NULL


SELECT AVG (EDAD) AS MEDIA_CON_COCHE
FROM Usuario 
WHERE COCHE IS NOT NULL

