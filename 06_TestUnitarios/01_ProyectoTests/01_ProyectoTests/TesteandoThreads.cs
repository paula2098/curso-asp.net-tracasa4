﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _01_ProyectoTests
{
    class TesteandoThreads
    {

        [Test]
        public void ProbandoHilos()
        {
            Console.WriteLine("Arrancando ProbandoHilos");
            Thread hilo1 = new Thread(FuncionHilo1);
            Thread hilo2 = new Thread(FuncionHilo2);
            hilo1.Start();
            hilo2.Start();
            Console.WriteLine("Terminando ProbandoHilos");
            Thread.Sleep(1000);
        }

        public static void FuncionHilo1()
        {
            Console.WriteLine("Arrancando Hilo1");
            for (int i = 0; i < 1000000; i++)
            {

            }
            Console.WriteLine("Terminando Hilo1");
        }
        
        public static void FuncionHilo2()
        {
            Console.WriteLine("Arrancando Hilo2");
            for (int i = 0; i < 1000000; i++)
            {

            }
            Console.WriteLine("Terminando Hilo2");
        }
    }
}
