﻿using System;

namespace Ejemplo05_Lambdas
{
    // Vamos a crear un nuevo tipo de dato, que en vez de 
    // almacenar información, almacene una función con 
    // determinada estructura ó firma.
    // La estructura es como la interfaz de una función,
    // pero sin importar el nombre: Los tipos de datos
    // que recibe, y el tipo de dato que devuelve
    // En C eran punteros a funciones, en Java hasta Java8,
    // Eran interfaces con una sóla función, en JS funciones flecha
    // En C# son los delegados
    delegate void FuncionQueRecibeInt(int param);

    class Program
    {
        static void Main(string[] args)
        {
            // LAMBDAS
            // Invocar funciones estataticas: 
            FuncionEstatatica(5);
            OtraEstatica(7);
            // Variables de tipo función (delegados)
            FuncionQueRecibeInt funRecint;
            funRecint = FuncionEstatatica;
            funRecint(200);
            // funRecint = null;    // provocaria una excepcion
            funRecint = OtraEstatica;
            funRecint(200);
            Console.WriteLine("\n\nOtra libreria, otro módulo, otro funcion, hace:");
            // OtroSistema(OtraEstatica);
            OtroSistema(FuncionEstatatica);
            OtroSistema(funRecint);
        }
        static void OtroSistema(FuncionQueRecibeInt funExt)
        {   // Queremos recibir una funcion como parametro:
            // LlamarFuncionExterna por ejemplo, que reciba un int
            int valor = 3;
            Console.WriteLine("Otro sistema hace sus cosas");
            System.Threading.Thread.Sleep(2000);
            Console.WriteLine("Tardar su tiempo");
            Console.WriteLine("Y llamar a la funcionalidad externa");
            funExt(3);
        }
        static void FuncionEstatatica(int x)
        {
            Console.WriteLine("No necesito un objeto para ser llamada: ");
            Console.WriteLine("Param: " + x);
        }
        static void OtraEstatica(int y)
        {
            Console.WriteLine(y + " - Otra Estatica: ");
        }
    }
}
