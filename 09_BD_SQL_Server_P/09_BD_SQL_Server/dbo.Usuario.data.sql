﻿SET IDENTITY_INSERT [dbo].[Usuario] ON
INSERT INTO [dbo].[Usuario] ( [email], [nombre],  [edad], [altura], [activo]) VALUES ( N'Usuario20', N'usuario20@gmail.com', 32, 170, 0)
INSERT INTO [dbo].[Usuario] ( [email], [nombre],  [edad], [altura], [activo]) VALUES ( N'Usuario30', N'usuario30@gmail.com', 88, 175, 1)
INSERT INTO [dbo].[Usuario] ([email],  [nombre],  [edad], [altura], [activo]) VALUES ( N'Usuario40', N'usuario40@gmail.com', 55, 161, 0)
INSERT INTO [dbo].[Usuario] ( [email], [nombre],  [edad], [altura], [activo]) VALUES ( N'Usuario56', N'usuario56@gmail.com', 22, 152, 0)
INSERT INTO [dbo].[Usuario] ([email], [nombre],  [edad], [altura], [activo]) VALUES ( N'Usuario60', N'usuario60@gmail.com', 52, 189, 1)
INSERT INTO [dbo].[Usuario] ([email], [nombre],  [edad], [altura], [activo]) VALUES ( N'Usuario85', N'usuario85@gmail.com', 52, 167, 1)
INSERT INTO [dbo].[Usuario] ( [email],[nombre],  [edad], [altura], [activo]) VALUES ( N'Usuario78', N'usuario78@gmail.com', 52, 175, 1)
SET IDENTITY_INSERT [dbo].[Usuario] OFF
