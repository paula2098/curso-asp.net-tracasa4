﻿using System;

namespace Ejemplo05_Lambdas
{
    // Vamos a crear un nuevo tipo de dato que en vez de almacenar
    // informacion, almacene una funcion con determinada 
    // estructura o firma
    // La estructura es como la interfaz de una funcion,
    // pero sin importar el nombre: los tipos de datos
    // que recibe, y el tipo de datos que devuelve
    // En C eran punteros a funciones, en java hasta java8,
    // Era interfaces con una sola funcion, en JS funciones
    // C# son los delegados

    delegate void FuncionQueRecibeInt(int x);





    class Program
    {
        static void Main(string[] args)
        {
            FuncionEstatica(5);
            OtraEstatica(7);
            // Variables de tipo funcion (delegados)
            FuncionQueRecibeInt funRecint;
            funRecint = FuncionEstatica;
            funRecint(200);
            funRecint = OtraEstatica;
            funRecint(200);
            Console.WriteLine("Otra libreria, otro modulo, otro funcion, hace: ");
            OtroSistema(FuncionEstatica);

        }

        static void OtroSistema()
        {
            /*
             
             */
            int valor = 3;
            //  LlamarFuncionExterna(3);
            Console.WriteLine("Otro sistema hace sus cosas");
            System.Threading.Thread.Sleep(2000);
            Console.WriteLine("Tardar su tiempo");
            Console.WriteLine("Y llamar a la funcionalidad externa");

        }


        static void FuncionEstatica(int x)
        {
            Console.WriteLine("No necesito un objeto para ser llamada");
            Console.WriteLine("Param: "+x);
        }
        
        static void OtraEstatica(int y)
        {
            Console.WriteLine(y + " - Otra estatica: ");
        }
    }
}
