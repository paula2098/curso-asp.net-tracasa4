﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio06_Callbk_Deleg
{
    public static class CalculaArrays
    {
        // public delegate float FuncionOperacion(float x, float y);
        
        public static float CalculaArray(float[] nums, Func<float, float, float> funOpera)
        {
            
            if (nums == null)
                throw new Exception("El array no puede ser null");
            else if (nums.Length == 0)
                throw new Exception("El array no puede estar vacio");
            else if (funOpera == null)
                throw new Exception("La funcion no puede estar vacia");

            float resultado = nums[0];
            for (int i = 1; i < nums.Length; i++)
            {
                resultado = funOpera.Invoke(resultado, nums[i]);
            }
            return resultado;
        }
    }
}
