﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio06_Callback
{
    // Delegado
    delegate float FuncionRes(float[] arrayOp, int x);


    static class VistaCalculadora
    {
        // Pedimos al usuario por teclado los elementos del array
        // y los almacenamos
        public static float[] pedirDatos() 
        {

            Console.Write("Introduce el número de operandos del array: ");
            int x = int.Parse(Console.ReadLine());
            float[] arrayOp = new float[x];
            Console.WriteLine("Introduce los operandos uno a uno por teclado: ");

            for (int i = 0; i < arrayOp.Length; i++)
            {
                Console.Write("Introduce el elemento número " + i + ": ");
                arrayOp[i] = float.Parse(Console.ReadLine());
            }
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("");
            Console.WriteLine("Elementos almacenados en el array correctamente.");
            Console.ForegroundColor = ConsoleColor.White;

            return arrayOp;
        }

        // Pedimos al usuario por teclado la operación deseada
        public static int pedirOperacion()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("------Seleccione una opción------");
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine(" 1 - Suma");
            Console.WriteLine(" 2 - Resta");
            Console.WriteLine(" 3 - Multiplicación");
            Console.WriteLine(" 4 - División");
            Console.ForegroundColor = ConsoleColor.Green;

            Console.WriteLine("----------------------------------");
            Console.ForegroundColor = ConsoleColor.White;

            int op = int.Parse(Console.ReadLine());

            do
            {
                switch (op)
                {
                    case 1:
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("");
                        Console.WriteLine("Opción elegida: SUMA");
                        Console.WriteLine("");
                        Console.ForegroundColor = ConsoleColor.White;


                        return 1;

                    case 2:
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("");
                        Console.WriteLine("Opción elegida: RESTA");
                        Console.WriteLine("");
                        Console.ForegroundColor = ConsoleColor.White;
                        return 2;

                    case 3:
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("");
                        Console.WriteLine("Opción elegida: MULTIPLICACIÓN");
                        Console.WriteLine("");
                        Console.ForegroundColor = ConsoleColor.White;
                        return 3;

                    case 4:
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("");
                        Console.WriteLine("Opción elegida: DIVISIÓN");
                        Console.WriteLine("");
                        Console.ForegroundColor = ConsoleColor.White;
                        return 4;

                    default:

                        Console.WriteLine("Introduzca opcion válida");
                        break;

                }
                return 0;
            } while (op != 0) ;
            

        }

        public static float CalcularResultado (FuncionRes resultado)
        {
            int operadorElegido; // Int que almacenará la opción de operación

            float[] arrayOp; // Array

            operadorElegido = pedirOperacion(); // Guardamos la operacion que quiere realizar

            arrayOp = pedirDatos(); // Guardamos los elementos del array introducido por el usuario

            return resultado(arrayOp, operadorElegido); // Devolvemos el array y el operador
;        }





    }


}
