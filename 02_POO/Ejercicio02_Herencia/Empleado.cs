﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio02_Herencia
{ 
    public class Empleado : Ejercicio01_Encapsulacion.Usuario
    {
        float salario;


        public Empleado() 
        {
            
        }

        public Empleado(string nombre, int edad, float altura, float salario) 
            : base(nombre, edad, altura)
        {
            this.Salario = salario;
        }

        public float Salario { get => salario; 
            set => salario = (value < 7000) ? 7000 : value ;
        }

        public override string ToString()
        {
            return base.ToString().Replace("Usuario", "Empleado") 
                + ", " + this.Salario + " EUR  ";
        }
    }
}
