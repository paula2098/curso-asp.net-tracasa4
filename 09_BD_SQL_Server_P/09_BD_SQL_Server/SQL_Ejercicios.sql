﻿--1
SELECT SUM(C.Cantidad)
FROM Producto AS P INNER JOIN CompraUsuarioProducto AS C ON C.IdProducto = P.Id INNER JOIN Usuario AS U ON C.IdUsuario = U.Id
WHERE U.Edad <= 20

--2
SELECT U.Nombre AS Usuario, P.Nombre AS Producto, C.Cantidad, P.Precio AS 'Precio unidad', (C.Cantidad * P.Precio) AS 'Precio total'
FROM Producto AS P INNER JOIN CompraUsuarioProducto AS C ON C.IdProducto = P.Id INNER JOIN Usuario AS U ON C.IdUsuario = U.Id
--3

SELECT SUM(C.Cantidad * P.Precio) AS 'Ventas en euros'
FROM Producto AS P INNER JOIN CompraUsuarioProducto AS C ON C.IdProducto = P.Id INNER JOIN Usuario AS U ON C.IdUsuario = U.Id
WHERE YEAR(Fecha) = '2020'
--4
SELECT U.Nombre FROM Producto AS P INNER JOIN CompraUsuarioProducto AS C ON C.IdProducto = P.Id INNER JOIN Usuario AS U ON C.IdUsuario = U.Id
WHERE YEAR(Fecha) = '2019'
GROUP BY U.Nombre
--5
SELECT TOP 1 P.Nombre AS 'Producto mas vendido 2018'
FROM Producto AS P INNER JOIN CompraUsuarioProducto AS C ON C.IdProducto = P.Id INNER JOIN Usuario AS U ON C.IdUsuario = U.Id
WHERE YEAR(Fecha) = '2018' GROUP BY P.Nombre ORDER BY SUM(C.Cantidad) DESC
--6
SELECT TOP 1 P.Nombre AS 'Producto mas vendido 2019'
FROM Producto AS P INNER JOIN CompraUsuarioProducto AS C ON C.IdProducto = P.Id INNER JOIN Usuario AS U ON C.IdUsuario = U.Id
WHERE YEAR(Fecha) = '2019' GROUP BY P.Nombre ORDER BY SUM(C.Cantidad) DESC
--7
SELECT TOP 1 P.Nombre AS 'Producto mas vendido 2020'
FROM Producto AS P INNER JOIN CompraUsuarioProducto AS C ON C.IdProducto = P.Id INNER JOIN Usuario AS U ON C.IdUsuario = U.Id
WHERE YEAR(Fecha) = '2020' GROUP BY P.Nombre ORDER BY SUM(C.Cantidad) DESC
--8
SELECT * FROM (SELECT TOP 1 '2018' AS Año, P.Nombre AS 'Producto mas vendido'
FROM Producto AS P INNER JOIN CompraUsuarioProducto AS C ON C.IdProducto = P.Id INNER JOIN Usuario AS U ON C.IdUsuario = U.Id
WHERE YEAR(Fecha) = '2018' GROUP BY P.Nombre ORDER BY SUM(C.Cantidad) DESC) AS X
UNION ALL
SELECT * FROM (SELECT TOP 1 '2019' AS Año, P.Nombre AS 'Producto mas vendido'
FROM Producto AS P INNER JOIN CompraUsuarioProducto AS C ON C.IdProducto = P.Id INNER JOIN Usuario AS U ON C.IdUsuario = U.Id
WHERE YEAR(Fecha) = '2019' GROUP BY P.Nombre ORDER BY SUM(C.Cantidad) DESC) AS Y
UNION ALL
SELECT * FROM (SELECT TOP 1 '2020' AS Año, P.Nombre AS 'Producto mas vendido'
FROM Producto AS P INNER JOIN CompraUsuarioProducto AS C ON C.IdProducto = P.Id INNER JOIN Usuario AS U ON C.IdUsuario = U.Id
WHERE YEAR(Fecha) = '2020' GROUP BY P.Nombre ORDER BY SUM(C.Cantidad) DESC) AS Z
--9
SELECT (C.Marca + ' ' + C.Modelo) AS 'Coche del usuario mas gastador'
FROM Usuario AS U INNER JOIN Coche AS C ON U.IdCoche = C.Id
WHERE U.Nombre IN
(SELECT TOP 1 U.Nombre
FROM Producto AS P INNER JOIN CompraUsuarioProducto AS CUP ON CUP.IdProducto = P.Id INNER JOIN Usuario AS U ON CUP.IdUsuario = U.Id
WHERE U.IdCoche IS NOT NULL
GROUP BY U.Nombre ORDER BY COUNT(P.Precio * CUP.Cantidad) DESC)
--10
SELECT P.Nombre AS Producto, AVG(CAST(U.Edad AS FLOAT)) AS 'Media edad'
FROM Producto AS P INNER JOIN CompraUsuarioProducto AS C ON C.IdProducto = P.Id INNER JOIN Usuario AS U ON C.IdUsuario = U.Id
GROUP BY P.Nombre

SELECT U.Nombre AS Usuario, P.Nombre AS Producto, C.Marca AS Coche
FROM Producto AS P INNER JOIN CompraUsuarioProducto AS CUP ON CUP.IdProducto = P.Id INNER JOIN Usuario AS U ON CUP.IdUsuario = U.Id INNER JOIN Coche AS C ON U.IdCoche = C.Id
WHERE C.Marca = 'Mercedes'