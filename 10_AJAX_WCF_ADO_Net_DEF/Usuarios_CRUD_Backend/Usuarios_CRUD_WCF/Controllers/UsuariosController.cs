﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace ModeloUsuarios
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsuariosController : ControllerBase
    {
        IModeloGenerico<Usuario> modeloUsuario;

        private readonly ILogger<UsuariosController> _logger;

        public UsuariosController(ILogger<UsuariosController> logger)
        {
            _logger = logger;
            this.modeloUsuario = ModeloUsuario.Instancia;
        }
        [HttpPost]
        public void Post(Usuario nuevoObj)
        {
            modeloUsuario.Crear(nuevoObj);
        }

        [HttpGet]
        public IList<Usuario> Get()
        {
            return modeloUsuario.LeerTodos();
        }

        [HttpDelete]
        public bool Delete(int entero)
        {
            return modeloUsuario.Eliminar(entero);
        }
        // Tenemos que vincular el parametro entero
        // al PARÁMETRO DE RUTA (leer de la URL):
        // http://localhost:21902/usuarios/20
        // El 20 pasará a ser el entero
        [HttpGet("/{id}")]
        public Usuario Get(int id)
        {
            return modeloUsuario.LeerUno(id);
        }
        /* public bool LeerConfirmar(string nombre)
         {
             return modeloUsuario.LeerConfirmar(nombre);
         }*/

        [HttpPut]
        public void Put(Usuario usuario)
        {
            modeloUsuario.Modificar(usuario);
        }
    }
}
