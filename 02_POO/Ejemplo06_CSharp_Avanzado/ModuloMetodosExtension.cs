﻿using System;

namespace Ejemplo06_CSharp_Avanzado.MetodosExt
{

    // En c# podemos añadir metodos a una clase desde otras clases:

    public static class ModuloMetodosExtension
    {
        // Mediante una funcion estatica, con estructura:

        public static string FormatearNombre(this Fruta laFruta)
        {

            // MEtodo de extensioón
            string nombreFormateado = "FRUTA " + laFruta.Nombre.ToUpper();
            return nombreFormateado;
        }
        public static string FormatearNombreN(Fruta laFruta)
        {

            // MEtodo de extensioón
            string nombreFormateado = "FRUTA " + laFruta.Nombre.ToUpper();
            return nombreFormateado;
        }
        public static string FormatearNombre(this Fruta laFruta, string formato)
        {

            // MEtodo de extensioón
            string nombreFormateado = "FRUTA " + laFruta.Nombre.ToUpper();
            return nombreFormateado;
        }

        public static string ToUpperString(this object cualquierObjeto)
        {
            return cualquierObjeto.ToString().ToUpper();
        }

        public static string ToUpperString(this Array cualquierArray)
        {
            string result = "Array " + cualquierArray.ToString().ToUpper();
            foreach (object elem in cualquierArray)
            {
                result += "\n - " + elem.ToUpperString();
            }
            return result;
        }
        
        public static string ToUpperFiltrado(this Array cualquierArray, string cadenaFiltro)
        {
            string result = "Array " + cualquierArray.ToString().ToUpper();
            foreach (object elem in cualquierArray)
            {
                if (elem.ToString().ToUpper().Contains(cadenaFiltro.ToUpper()))
                {
                    result += "\n - " + elem.ToUpperString();
                }
            }
            return result;
        }
    }
}
