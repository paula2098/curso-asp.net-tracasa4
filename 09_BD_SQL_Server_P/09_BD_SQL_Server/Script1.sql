﻿-- El conjunto común de datos
/*
SELECT	U.Id, U.email, 
		C.Id, C.Marca
	FROM Usuario as U
	INNER JOIN Coche as C
	ON U.idCoche = C.Id
	*/
-- Todos los usuarios y si tienen coche, con su coche. Y si no, se rellena lo del coche como NULL
/*SELECT	U.Id, U.email, 
		C.Id, C.Marca
  FROM Usuario as U
			LEFT JOIN Coche as C
	ON U.idCoche = C.Id */

-- Todos los coches con sus usuarios si los tuviera, y si no también
/*SELECT	U.Id, U.email, 
	C.Id, C.Marca
	FROM Usuario as U
		RIGHT JOIN Coche as C
ON U.idCoche = C.Id
*/
-- Todo con todo aunque no tengan combinación

/*SELECT	U.Id, U.email, 
	C.Id, C.Marca
	FROM Usuario as U
		FULL OUTER JOIN Coche as C
ON U.idCoche = C.Id
*/


   
/*
	 select SUM(c.Cantidad) from Producto as P, CompraUsuarioProducto as C, Usuario as U 
			where C.IdUsuario = U.Id and C.IdProducto = P.Id and U.edad <= 20 
		1 - Esta consulta con JOIN
		
	Por cada usuario, nombre de usuario y nombre de producto,
		2 - El precio del producto de sus compras
		3 - Cuanto se ha gastado en total en cada producto
		4 - Cuantas copias del producto ha comprado
	Por otro lado:
		5 - Todas las ventas (dinero) del 2020
		6 - Todos los usuarios que compraron en el 2019
		7 - Qué productos se han vendido mejor a lo largo del 2018, 2019 y 2020
		8 - El coche cuyo propietario ha comprado más a lo largo de la historia
		9 - La media de edad de los compradores de cada uno de los productos (melón, cuajada...)
		10 - Qué productos han comprado los usuarios del Mercedes
*/
/*
--2 - El precio del producto de sus compras
SELECT
FROM  
WHERE

--3 - Cuanto se ha gastado en total en cada producto

SELECT
FROM
WHERE


--4 - Cuantas copias del producto ha comprado

SELECT
FROM
WHERE


--5 - Todas las ventas (dinero) del 2020

SELECT
FROM
WHERE


--6 - Todos los usuarios que compraron en el 2019

SELECT
FROM
WHERE


--7 - Qué productos se han vendido mejor a lo largo del 2018, 2019 y 2020

SELECT
FROM
WHERE


--8 - El coche cuyo propietario ha comprado más a lo largo de la historia

SELECT
FROM
WHERE


--9 - La media de edad de los compradores de cada uno de los productos (melón, cuajada...)

SELECT
FROM
WHERE


--10 - Qué productos han comprado los usuarios del Mercedes
*/
/*SELECT
FROM
WHERE;
*/
-- CUANTOS USUARIOS TIENEN CADA COCHE
/*SELECT C.MARCA
FROM COCHE  C INNER JOIN USUARIO U ON U.IDCOCHE = C.ID
GROUP BY C.MARCA;*/

SELECT *
FROM COCHE
