﻿using System;

namespace Ejemplo06_IfElse
{
    class Program
    {
        static void Main(string[] args)
        {
            string linea;
            /*int num1, num2;
            Console.Write("Ingrese primer valor:");
            linea = Console.ReadLine();
            num1 = int.Parse(linea);
            Console.Write("Ingrese segundo valor:");
            linea = Console.ReadLine();
            num2 = int.Parse(linea);
            if (num1 < num2)
                Console.Write("El mayor es " + num1);
            else
            {
                Console.Write("El mayor es " + num2);
                Console.Write("");
            }

            Console.ReadKey();
            */

            int n, x;
            Console.Write("Ingrese el valor final:");
            linea = Console.ReadLine();
            n = int.Parse(linea);
            x = 1;
            bool cond = x <= n;
            while (cond)
            {
                Console.Write(x);
                x = x + 1;
                cond = x <= n;
                if (x == 20)
                    continue;

                Console.Write(" - ");

            }
            Console.ReadKey();
        }
    }
}
