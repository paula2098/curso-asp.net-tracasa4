﻿using System;

namespace Ejercicio06_Callback
{

    // Ejercicio: Crear dos sistemas (clases independientes que no se conocen entre si)
    // La primera tendra 4 funciones para sumar, restar, mult y div
    // todo con float. Estas podran realizar la operacion sobre un array:
    // {9, 7, 5, 3} -> suma 24    resta -> -6     mult -> 945
    // {42, 3, 2} div (42 / 3) / 2 -> 7     suma -> 47
    // {3} -> cualquier operador devuelve 3
    // No puede estar vacio el array
    // La segunda clase, le piede al usuario cuantos operandos habrá,
    //y los operandos uno a uno, la operación, y mostrará el resultado
    // Si alguien quiere

    class Program
    {


        static void Main(string[] args)
        {
            // Calculo resultado final a partir de un array de float (suma, resta, multiplicacion, division)

            float resultadoFinal = VistaCalculadora.CalcularResultado (CalculaArrays.OperacionCompleta);
            Console.WriteLine("");
            Console.WriteLine("El resultado final es: " + resultadoFinal);
        }
    }
}
