﻿using Ejemplo03_MVC.Aplicacion.Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo03_MVC.Aplicacion.Modelo.Interfaces
{
    interface IModeloMuestra : IModeloGenerico<Muestra>
    {
    }
}
